var express = require('express');
var router = express.Router();
var boardModel = require('../models/board');

router.get('/', (req, res) => {
  boardModel.find({}).sort({date: -1}).exec((err, boardContents) => {
    if (err) throw err;
    res.render('board', {title: 'Board', contents: boardContents});
  });
});

router.post('/', (req, res) => {

  var boardContent = new boardModel();
  boardContent.title = req.body.addContentSubject;
  boardContent.author = req.body.addContentAuthor;
  boardContent.content = req.body.addContents;

  boardContent.save((err) => {
    if (err) throw err;
    res.redirect('/board');
  });

});

router.get('/view', (req, res) => {
  var contentId = req.param('id');
  boardModel.findOne({_id: contentId}, (err, boardContent) => {
    if (err) throw err;
    res.render('detail', {title: 'Board', content: boardContent});
  });
});

module.exports = router;
