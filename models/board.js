var mongoose = require('mongoose');

var boardSchema = mongoose.Schema({
  author: String,
  title: String,
  content: String,
  published_date: {type: Date, default: Date.now},
  updated_date: {type: Date, default: Date.now},
  deleted: {type: Boolean, default: false}
});

module.exports = mongoose.model('Board', boardSchema);
